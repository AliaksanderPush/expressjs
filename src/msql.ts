import { UserModel } from './model/user.model';
import { Sequelize } from 'sequelize-typescript';
import { Role } from './model/role.model';

import dotenv from 'dotenv';

dotenv.config();

const sequelize = new Sequelize({
	database: process.env.MYSQL_DATABASE,
	dialect: 'mysql',
	username: process.env.MYSQL_USER,
	password: process.env.MYSQL_PASSWORD,
	storage: ':memory:',
	models: [UserModel, Role],
});

export const connection = async () => {
	try {
		await sequelize.sync({ alter: true });
		console.log('Database-----------OK!');
	} catch (err) {
		console.error('Unable to connect to the database!!!:', err);
	}
};
