export interface IUsers {
	id?: string | number;
	name: string;
	password: string;
	profession: string;
	age: number;
	email: string;
}
