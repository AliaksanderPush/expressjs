import { IsEmail, Min, Max, IsInt, Length, IsString } from 'class-validator';

export class UserRegisterDto {
	id?: string | number;
	@IsString({ message: 'Введите имя' })
	@Length(2, 20)
	name: string;
	@IsString({ message: 'Введите пароль' })
	@Length(2, 20)
	password: string;
	profession: string;
	@IsInt()
	@Min(0)
	@Max(99)
	age: number;
	@IsEmail({}, { message: 'Неверно указан email' })
	email: string;
}
