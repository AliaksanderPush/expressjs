import multer, { Options } from 'multer';
import { resolve, extname } from 'path';
import { v4 as uuidv4 } from 'uuid';

export const storageConfig = {
	dest: resolve(__dirname, '..', 'assets'),
	storage: multer.diskStorage({
		destination: (request, file, callback) => {
			callback(null, resolve(__dirname, '..', 'assets'));
		},
		filename: (req, file, cb) => {
			const hash = uuidv4();
			cb(null, hash + extname(file.originalname));
		},
	}),

	limits: {
		fileSize: 5 * 1024 * 1024, // 5MB
	},
	fileFilter: (request, file, callback) => {
		const formats = ['image/jpeg', 'image/jpg', 'image/png', 'image/svg'];

		if (formats.includes(file.mimetype)) {
			callback(null, true);
		} else {
			callback(new Error('Такой формат не поддерживается'));
		}
	},
} as Options;
