import { UserModel } from './user.model';
import { IRole } from './../dto/role.dto';
import {
	Table,
	Column,
	Model,
	HasMany,
	AutoIncrement,
	PrimaryKey,
	AllowNull,
	NotEmpty,
} from 'sequelize-typescript';

@Table({
	tableName: 'role',
	timestamps: false,
})
export class Role extends Model implements IRole {
	@AutoIncrement
	@PrimaryKey
	@Column
	id: number;

	@AllowNull
	@NotEmpty
	@Column
	value: string;

	@HasMany(() => UserModel)
	users: UserModel[];
}
