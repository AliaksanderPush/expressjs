import {
	Table,
	Column,
	Model,
	AutoIncrement,
	PrimaryKey,
	AllowNull,
	NotEmpty,
	ForeignKey,
	BelongsTo,
} from 'sequelize-typescript';
import { IUsers } from '../dto/user.dto';
import { Role } from './role.model';

@Table({
	tableName: 'user',
	timestamps: false,
})
export class UserModel extends Model implements IUsers {
	@AutoIncrement
	@PrimaryKey
	@Column
	id?: number;

	@AllowNull
	@NotEmpty
	@Column
	name!: string;

	@AllowNull
	@NotEmpty
	@Column
	password!: string;

	@AllowNull
	@Column
	profession: string;

	@AllowNull
	@Column
	age: number;

	@AllowNull
	@NotEmpty
	@Column
	email!: string;

	@ForeignKey(() => Role)
	@Column
	roleId: number;

	@BelongsTo(() => Role)
	role: Role;
}
