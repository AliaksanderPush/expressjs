import { IRole } from './../dto/role.dto';
import { AuthGuard } from './../middleWares/auth.guard';
import { UserService } from './../service/user.service';
import { Request, Response, NextFunction } from 'express';
import { BaseController } from '../common/base.controller';
import { injectable, inject } from 'inversify';
import { TYPES } from '../types';
import { UserLoginDto } from '../dto/userLogin.dto';
import { UserRegisterDto } from '../dto/userRegister.dto';
import { sign } from 'jsonwebtoken';
import { ValidateMidleWare } from '../middleWares/validate.middleware';
import { UploadMiddleWare } from '../middleWares/upload.middleware';
import { RoleMidleware } from '../middleWares/roles.midleware';
import 'reflect-metadata';

@injectable()
export class UserController extends BaseController {
	constructor(@inject(TYPES.UserService) private userService: UserService) {
		super();
		this.bindRouters([
			{ path: '/user', methot: 'get', func: this.getUsers },
			{ path: '/user', methot: 'post', func: this.addUser },
			{
				path: '/user/:id',
				methot: 'put',
				func: this.updateUser,
				middlewares: [new RoleMidleware('admin')],
			},
			{
				path: '/user/:id',
				methot: 'delete',
				func: this.removeUser,
				middlewares: [new RoleMidleware('admin')],
			},
			{ path: '/user/:id', methot: 'get', func: this.refreshUser },
			{
				path: '/login',
				methot: 'post',
				func: this.login,
				middlewares: [new ValidateMidleWare(UserLoginDto)],
			},

			{
				path: '/register',
				methot: 'post',
				func: this.register,
				middlewares: [new ValidateMidleWare(UserRegisterDto)],
			},

			{
				path: '/info',
				methot: 'get',
				func: this.info,
				middlewares: [new AuthGuard()],
			},
			{
				path: '/upload',
				methot: 'post',
				func: this.fileLoader,
				middlewares: [new UploadMiddleWare()],
			},
		]);
	}

	async getUsers(req: Request, res: Response, next: NextFunction) {
		const result = await this.userService.getAll();
		res.json(result);
	}

	async addUser(req: Request, res: Response, next: NextFunction) {
		const { password } = req.body;
		await this.userService.addUsers(req.body, password);
		this.ok(res, 'Пользователь добавлен успешно');
	}

	async updateUser(req: Request, res: Response, next: NextFunction) {
		const id = req.params.id;
		const data = req.body;
		await this.userService.putUser(+id, data);
		this.ok(res, 'Пользователь успешно изменен!');
	}

	async removeUser(req: Request, res: Response, next: NextFunction) {
		const id = req.params.id;
		await this.userService.deleteUser(+id);
		this.ok(res, 'Пользователь успешно удален!');
	}

	async refreshUser(req: Request, res: Response, next: NextFunction) {
		const id = req.params.id;
		const user = await this.userService.getUser(+id);
		res.json(user);
	}

	async login(
		req: Request<{}, {}, UserLoginDto>,
		res: Response,
		next: NextFunction,
	): Promise<void> {
		const { email, password } = req.body;
		const searchUser = await this.userService.searchByEmail(email);
		let role = '';
		if (searchUser) {
			const roleId = searchUser.roleId;
			role = await this.userService.searchByRole(roleId);
			const result = await this.userService.comparePassword(password, searchUser.password);

			if (!result) {
				this.send(res, 400, 'Пароль указан неверно!');
			}
		} else {
			this.send(res, 400, 'Указанный пользователь не найден! Зарегистрируйтесь!');
		}

		const secret = process.env.SECRET;

		const jwt = this.signJWT(req.body.email, role as string, secret as string);

		this.ok(res, { jwt });
	}
	async register(
		req: Request<{}, {}, UserRegisterDto>,
		res: Response,
		next: NextFunction,
	): Promise<void> {
		const { email, password } = req.body;
		const result = await this.userService.searchByEmail(email);

		if (!result) {
			this.userService.addUsers(req.body, password);
			this.ok(res, 'Пользователь успешно зарегистрирован!');
		} else {
			this.send(res, 401, 'Этот пользователь уже зарегистрирован!');
		}
	}

	async info({ user }: Request, res: Response, next: NextFunction): Promise<void> {
		this.ok(res, { email: user.email, role: user.role });
	}

	signJWT(email: string, role: string, secret: string): string {
		const payLoad = {
			email,
			role,
		};

		return sign(payLoad, secret, { expiresIn: '24h' });
	}

	fileLoader(req: Request, res: Response, next: NextFunction) {
		this.send(res, 401, 'Файл загружен');
	}
}
