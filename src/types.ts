export const TYPES = {
	App: Symbol.for('App'),
	UserController: Symbol.for('UserController'),
	UserService: Symbol.for('UserService'),
	UserModel: Symbol.for('UserModel'),
};
