import { IRole } from './../dto/role.dto';
import { Role } from './../model/role.model';
import { IUsers } from './../dto/user.dto';
import { UserModel } from '../model/user.model';
import { injectable } from 'inversify';
import { hash, compare } from 'bcryptjs';
import 'reflect-metadata';

@injectable()
export class UserService {
	async getAll() {
		try {
			return await UserModel.findAll();
		} catch (err) {
			return Promise.reject(err);
		}
	}

	async addUsers(param: IUsers, password: string): Promise<void> {
		const { name, profession, age, email } = param;

		const hashPass = await hash(password, 7);
		try {
			const role: Role = await Role.create({ value: `user` });
			const { id } = role;

			await UserModel.create({
				name,
				profession,
				age,
				email,
				roleId: id,
				password: hashPass,
			});
		} catch (err) {
			return Promise.reject(err);
		}
	}

	async addRole(val: string): Promise<void> {
		try {
			await Role.create({ value: val });
		} catch (err) {
			return Promise.reject(err);
		}
	}

	async searchByEmail(email: string): Promise<UserModel | null> {
		return await UserModel.findOne({ where: { email } });
	}

	async searchByRole(id: number): Promise<string> {
		const role = await Role.findOne({ where: { id } });
		return role?.value as string;
	}

	async putUser(userId: number, data: UserModel) {
		try {
			await UserModel.update(data, { where: { id: userId } });
		} catch (err) {
			return Promise.reject(err);
		}
	}

	async deleteUser(id: number) {
		try {
			await UserModel.destroy({ where: { id } });
		} catch (err) {
			return Promise.reject(err);
		}
	}

	async getUser(id: number) {
		try {
			return await UserModel.findOne({ where: { id } });
		} catch (err) {
			return Promise.reject(err);
		}
	}

	async comparePassword(pass: string, hash: string): Promise<boolean> {
		return await compare(pass, hash);
	}
}
