"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseController = exports.router = void 0;
var express_1 = require("express");
var user_controller_1 = require("../controllers/user.controller");
var getUsers = user_controller_1.userController.getUsers, addUser = user_controller_1.userController.addUser, updateUser = user_controller_1.userController.updateUser, removeUser = user_controller_1.userController.removeUser, refresh = user_controller_1.userController.refresh;
exports.router = (0, express_1.Router)();
//router.get("/user", getUsers);
//router.post("/user", addUser);
//router.put("/user/:id", updateUser);
//router.delete("/user/:id", removeUser);
//router.get("/refresh", refresh);
var BaseController = /** @class */ (function () {
    function BaseController() {
        this._router = (0, express_1.Router)();
    }
    Object.defineProperty(BaseController.prototype, "router", {
        get: function () {
            return this._router;
        },
        enumerable: false,
        configurable: true
    });
    BaseController.prototype.send = function (res, code, message) {
        res.status(code);
        return res.type("application/json").json(message);
    };
    BaseController.prototype.ok = function (res, message) {
        return this.send(res, 200, message);
    };
    BaseController.prototype.created = function (res) {
        return res.sendStatus(201);
    };
    BaseController.prototype.bindRouters = function (routers) {
        for (var _i = 0, routers_1 = routers; _i < routers_1.length; _i++) {
            var router_1 = routers_1[_i];
            console.log("[".concat(router_1.methot, "] ").concat(router_1.path));
            var handler = router_1.func.bind(this);
            this.router[router_1.methot](router_1.path, handler);
        }
    };
    return BaseController;
}());
exports.BaseController = BaseController;
