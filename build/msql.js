"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.connection = void 0;
const user_model_1 = require("./model/user.model");
const sequelize_typescript_1 = require("sequelize-typescript");
const role_model_1 = require("./model/role.model");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const sequelize = new sequelize_typescript_1.Sequelize({
    database: process.env.MYSQL_DATABASE,
    dialect: 'mysql',
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    storage: ':memory:',
    models: [user_model_1.UserModel, role_model_1.Role],
});
const connection = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield sequelize.sync({ alter: true });
        console.log('Database-----------OK!');
    }
    catch (err) {
        console.error('Unable to connect to the database!!!:', err);
    }
});
exports.connection = connection;
