"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.users = void 0;
var uuid_1 = require("uuid");
exports.users = [
    { id: (0, uuid_1.v1)(), name: 'Nic', profession: 'developer', age: 30 },
    { id: (0, uuid_1.v1)(), name: 'Kate', profession: 'director', age: 27 },
    { id: (0, uuid_1.v1)(), name: 'Mike', profession: 'developer', age: 23 },
    { id: (0, uuid_1.v1)(), name: 'Ann', profession: 'manager', age: 32 },
];
