"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const auth_guard_1 = require("./../middleWares/auth.guard");
const user_service_1 = require("./../service/user.service");
const base_controller_1 = require("../common/base.controller");
const inversify_1 = require("inversify");
const types_1 = require("../types");
const userLogin_dto_1 = require("../dto/userLogin.dto");
const userRegister_dto_1 = require("../dto/userRegister.dto");
const jsonwebtoken_1 = require("jsonwebtoken");
const validate_middleware_1 = require("../middleWares/validate.middleware");
const upload_middleware_1 = require("../middleWares/upload.middleware");
const roles_midleware_1 = require("../middleWares/roles.midleware");
require("reflect-metadata");
let UserController = class UserController extends base_controller_1.BaseController {
    constructor(userService) {
        super();
        this.userService = userService;
        this.bindRouters([
            { path: '/user', methot: 'get', func: this.getUsers },
            { path: '/user', methot: 'post', func: this.addUser },
            {
                path: '/user/:id',
                methot: 'put',
                func: this.updateUser,
                middlewares: [new roles_midleware_1.RoleMidleware('admin')],
            },
            {
                path: '/user/:id',
                methot: 'delete',
                func: this.removeUser,
                middlewares: [new roles_midleware_1.RoleMidleware('admin')],
            },
            { path: '/user/:id', methot: 'get', func: this.refreshUser },
            {
                path: '/login',
                methot: 'post',
                func: this.login,
                middlewares: [new validate_middleware_1.ValidateMidleWare(userLogin_dto_1.UserLoginDto)],
            },
            {
                path: '/register',
                methot: 'post',
                func: this.register,
                middlewares: [new validate_middleware_1.ValidateMidleWare(userRegister_dto_1.UserRegisterDto)],
            },
            {
                path: '/info',
                methot: 'get',
                func: this.info,
                middlewares: [new auth_guard_1.AuthGuard()],
            },
            {
                path: '/upload',
                methot: 'post',
                func: this.fileLoader,
                middlewares: [new upload_middleware_1.UploadMiddleWare()],
            },
        ]);
    }
    getUsers(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield this.userService.getAll();
            res.json(result);
        });
    }
    addUser(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const { password } = req.body;
            yield this.userService.addUsers(req.body, password);
            this.ok(res, 'Пользователь добавлен успешно');
        });
    }
    updateUser(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.params.id;
            const data = req.body;
            yield this.userService.putUser(+id, data);
            this.ok(res, 'Пользователь успешно изменен!');
        });
    }
    removeUser(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.params.id;
            yield this.userService.deleteUser(+id);
            this.ok(res, 'Пользователь успешно удален!');
        });
    }
    refreshUser(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.params.id;
            const user = yield this.userService.getUser(+id);
            res.json(user);
        });
    }
    login(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, password } = req.body;
            const searchUser = yield this.userService.searchByEmail(email);
            let role = '';
            if (searchUser) {
                const roleId = searchUser.roleId;
                role = yield this.userService.searchByRole(roleId);
                const result = yield this.userService.comparePassword(password, searchUser.password);
                if (!result) {
                    this.send(res, 400, 'Пароль указан неверно!');
                }
            }
            else {
                this.send(res, 400, 'Указанный пользователь не найден! Зарегистрируйтесь!');
            }
            const secret = process.env.SECRET;
            const jwt = this.signJWT(req.body.email, role, secret);
            this.ok(res, { jwt });
        });
    }
    register(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, password } = req.body;
            const result = yield this.userService.searchByEmail(email);
            if (!result) {
                this.userService.addUsers(req.body, password);
                this.ok(res, 'Пользователь успешно зарегистрирован!');
            }
            else {
                this.send(res, 401, 'Этот пользователь уже зарегистрирован!');
            }
        });
    }
    info({ user }, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ok(res, { email: user.email, role: user.role });
        });
    }
    signJWT(email, role, secret) {
        const payLoad = {
            email,
            role,
        };
        return (0, jsonwebtoken_1.sign)(payLoad, secret, { expiresIn: '24h' });
    }
    fileLoader(req, res, next) {
        this.send(res, 401, 'Файл загружен');
    }
};
UserController = __decorate([
    (0, inversify_1.injectable)(),
    __param(0, (0, inversify_1.inject)(types_1.TYPES.UserService)),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
exports.UserController = UserController;
