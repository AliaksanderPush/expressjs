"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.storageConfig = void 0;
const multer_1 = __importDefault(require("multer"));
const path_1 = require("path");
const uuid_1 = require("uuid");
exports.storageConfig = {
    dest: (0, path_1.resolve)(__dirname, '..', 'assets'),
    storage: multer_1.default.diskStorage({
        destination: (request, file, callback) => {
            callback(null, (0, path_1.resolve)(__dirname, '..', 'assets'));
        },
        filename: (req, file, cb) => {
            const hash = (0, uuid_1.v4)();
            cb(null, hash + (0, path_1.extname)(file.originalname));
        },
    }),
    limits: {
        fileSize: 5 * 1024 * 1024, // 5MB
    },
    fileFilter: (request, file, callback) => {
        const formats = ['image/jpeg', 'image/jpg', 'image/png', 'image/svg'];
        if (formats.includes(file.mimetype)) {
            callback(null, true);
        }
        else {
            callback(new Error('Такой формат не поддерживается'));
        }
    },
};
