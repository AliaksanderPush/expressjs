"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const role_model_1 = require("./../model/role.model");
const user_model_1 = require("../model/user.model");
const inversify_1 = require("inversify");
const bcryptjs_1 = require("bcryptjs");
require("reflect-metadata");
let UserService = class UserService {
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield user_model_1.UserModel.findAll();
            }
            catch (err) {
                return Promise.reject(err);
            }
        });
    }
    addUsers(param, password) {
        return __awaiter(this, void 0, void 0, function* () {
            const { name, profession, age, email } = param;
            const hashPass = yield (0, bcryptjs_1.hash)(password, 7);
            try {
                const role = yield role_model_1.Role.create({ value: `user` });
                const { id } = role;
                yield user_model_1.UserModel.create({
                    name,
                    profession,
                    age,
                    email,
                    roleId: id,
                    password: hashPass,
                });
            }
            catch (err) {
                return Promise.reject(err);
            }
        });
    }
    addRole(val) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield role_model_1.Role.create({ value: val });
            }
            catch (err) {
                return Promise.reject(err);
            }
        });
    }
    searchByEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield user_model_1.UserModel.findOne({ where: { email } });
        });
    }
    searchByRole(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const role = yield role_model_1.Role.findOne({ where: { id } });
            return role === null || role === void 0 ? void 0 : role.value;
        });
    }
    putUser(userId, data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield user_model_1.UserModel.update(data, { where: { id: userId } });
            }
            catch (err) {
                return Promise.reject(err);
            }
        });
    }
    deleteUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield user_model_1.UserModel.destroy({ where: { id } });
            }
            catch (err) {
                return Promise.reject(err);
            }
        });
    }
    getUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield user_model_1.UserModel.findOne({ where: { id } });
            }
            catch (err) {
                return Promise.reject(err);
            }
        });
    }
    comparePassword(pass, hash) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield (0, bcryptjs_1.compare)(pass, hash);
        });
    }
};
UserService = __decorate([
    (0, inversify_1.injectable)()
], UserService);
exports.UserService = UserService;
