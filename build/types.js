"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TYPES = void 0;
exports.TYPES = {
    App: Symbol.for('App'),
    UserController: Symbol.for('UserController'),
    UserService: Symbol.for('UserService'),
    UserModel: Symbol.for('UserModel'),
};
