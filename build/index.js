"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_service_1 = require("./service/user.service");
const main_1 = require("./main");
const user_controller_1 = require("./controllers/user.controller");
const inversify_1 = require("inversify");
const types_1 = require("./types");
const msql_1 = require("./msql");
function bootstrap() {
    return __awaiter(this, void 0, void 0, function* () {
        const container = new inversify_1.Container();
        container.bind(types_1.TYPES.App).to(main_1.App);
        container.bind(types_1.TYPES.UserController).to(user_controller_1.UserController).inSingletonScope();
        container.bind(types_1.TYPES.UserService).to(user_service_1.UserService).inSingletonScope();
        const app = container.get(types_1.TYPES.App);
        yield app.init();
        yield (0, msql_1.connection)();
    });
}
bootstrap();
