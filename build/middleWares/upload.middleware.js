"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadMiddleWare = void 0;
const multer_1 = __importDefault(require("multer"));
const multer_config_1 = require("../configs/multer.config");
class UploadMiddleWare {
    execute(req, res, next) {
        const upload = (0, multer_1.default)(multer_config_1.storageConfig).single('filedata');
        upload(req, res, (err) => {
            if (err instanceof multer_1.default.MulterError) {
                console.log(err);
                res.status(400).send('Ошибка Multer при загрузке');
            }
            else if (err) {
                console.log(err);
                res.status(400).send(' При загрузке произошла неизвестная ошибка.');
            }
        });
        next();
    }
}
exports.UploadMiddleWare = UploadMiddleWare;
