"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthMiddleWare = void 0;
const jsonwebtoken_1 = require("jsonwebtoken");
class AuthMiddleWare {
    execute(req, res, next) {
        if (req.headers.authorization) {
            try {
                const token = req.headers.authorization.split(' ')[1];
                const decData = (0, jsonwebtoken_1.verify)(token, process.env.SECRET);
                const { email, role } = decData;
                req.user = { email, role };
                next();
            }
            catch (err) {
                res.status(403).json('Пользователь не авторизован');
            }
        }
        else {
            next();
        }
    }
}
exports.AuthMiddleWare = AuthMiddleWare;
