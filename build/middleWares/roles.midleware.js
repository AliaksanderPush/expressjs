"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleMidleware = void 0;
const jsonwebtoken_1 = require("jsonwebtoken");
class RoleMidleware {
    constructor(roles) {
        this.roles = roles;
        this.roles = roles;
    }
    execute(req, res, next) {
        if (req.headers.authorization) {
            try {
                const token = req.headers.authorization.split(' ')[1];
                const decData = (0, jsonwebtoken_1.verify)(token, process.env.SECRET);
                const { role } = decData;
                let hasRole = false;
                role.split(',').forEach((rol) => {
                    if (rol.includes(this.roles)) {
                        hasRole = true;
                    }
                });
                if (!hasRole) {
                    return res.status(403).json('У вас нет доступа');
                }
                next();
            }
            catch (err) {
                res.status(403).json('Пользователь не авторизован');
            }
        }
        else {
            next();
        }
    }
}
exports.RoleMidleware = RoleMidleware;
